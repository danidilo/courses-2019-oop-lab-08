package it.unibo.oop.lab.mvcio;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import jdk.javadoc.internal.doclets.toolkit.util.DocFinder.Output;

/**
 * 
 */
public class Controller {

    private File file;
    private final String path;

    /**
     * 
     * @param path
     * Path file corrente
     */
    public Controller(final String path) {
        this.path = path;
        this.file = new File(path);
    }
    /**
     * 
     * @return the file
     */
    public File getFile() {
        return file;
    }
    /**
     * 
     * @param file
     * file to set
     */
    public void setFile(final File file) {
        this.file = file;
    }
    /**
     * 
     * @param whatToWrite
     * what we have to write on our current file
     */
    public void writeOnFile(final String whatToWrite) throws IOException {
        try {
            DataOutputStream output = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(this.file)));
            output.writeUTF(whatToWrite);
        } catch (Exception ex) {
            System.out.println("Error while opening the file or saving changes");
            throw new IOException();
        }
    }


    /*
     * This class must implement a simple controller responsible of I/O access. It
     * considers a single file at a time, and it is able to serialize objects in it.
     * 
     * Implement this class with:
     * 
     * 1) A method for setting a File as current file
     * 
     * 2) A method for getting the current File
     * 
     * 3) A method for getting the path (in form of String) of the current File
     * 
     * 4) A method that gets a String as input and saves its content on the current
     * file. This method may throw an IOException.
     * 
     * 5) By default, the current file is "output.txt" inside the user home folder.
     * A String representing the local user home folder can be accessed using
     * System.getProperty("user.home"). The separator symbol (/ on *nix, \ on
     * Windows) can be obtained as String through the method
     * System.getProperty("file.separator"). The combined use of those methods leads
     * to a software that runs correctly on every platform.
     */

}
